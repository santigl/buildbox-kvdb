/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxkvdb_keyvalueinmemorystorage.h>

#include <chrono>
#include <thread>

#include <gtest/gtest.h>

class KeyValueInMemoryStorageTestFixture : public ::testing::Test {
  protected:
    KeyValueInMemoryStorageTestFixture() {}

    buildboxkvdb::InMemoryKeyValueStorage storage;
};

TEST_F(KeyValueInMemoryStorageTestFixture, TestEmpty)
{
    ASSERT_EQ(storage.getKey("foo"), nullptr);
    ASSERT_EQ(storage.getKey("bar"), nullptr);

    ASSERT_EQ(storage.size(), 0);
}

TEST_F(KeyValueInMemoryStorageTestFixture, TestInsert)
{
    const auto key = "key";

    ASSERT_EQ(storage.getKey(key), nullptr);
    storage.putKey(key, "value");
    ASSERT_EQ(storage.size(), 1);

    const auto entry = storage.getKey(key);
    ASSERT_NE(entry, nullptr);
    ASSERT_EQ(*entry, "value");
}

TEST_F(KeyValueInMemoryStorageTestFixture, TestTTL)
{
    ASSERT_EQ(storage.getKey("key"), nullptr);
    storage.putKey("key", "value", std::chrono::seconds(60));

    std::this_thread::sleep_for(std::chrono::seconds(2));

    const auto entry = storage.getKey("key");
    ASSERT_NE(entry, nullptr);
    ASSERT_EQ(*entry, "value");
}

TEST_F(KeyValueInMemoryStorageTestFixture, TestCleanup)
{
    ASSERT_EQ(storage.getKey("key"), nullptr);
    storage.putKey("key1", "value1", std::chrono::seconds(1));
    storage.putKey("key2", "value2", std::chrono::seconds(1));

    storage.putKey("key3", "value3", std::chrono::seconds(60));

    std::this_thread::sleep_for(std::chrono::seconds(2));

    storage.deleteExpiredEntries();

    ASSERT_EQ(storage.size(), 1);
    ASSERT_EQ(*storage.getKey("key3"), "value3");
}
