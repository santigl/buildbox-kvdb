/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxkvdb_keyvalueinmemorystorage.h>
#include <buildboxkvdb_server.h>

#include <buildboxcommon_keyvaluestorageclient.h>
#include <buildboxcommon_temporarydirectory.h>

#include <gtest/gtest.h>

#include <memory>

class ServerTestFixture : public ::testing::Test {
  protected:
    ServerTestFixture()
        : serverAddress("unix://" + tmpDir.strname() + "/server.sock"),
          storage(), server(&storage), client(nullptr)
    {
        server.addListeningPort(serverAddress);
        server.start();

        buildboxcommon::ConnectionOptions connectionOptions;
        connectionOptions.setUrl(serverAddress);

        client = std::make_unique<buildboxcommon::KeyValueStorageClient>(
            connectionOptions);
    }

    ~ServerTestFixture()
    {
        server.shutdown();
        server.wait();
    }

    buildboxcommon::TemporaryDirectory tmpDir;
    const std::string serverAddress;

    buildboxkvdb::InMemoryKeyValueStorage storage;
    buildboxkvdb::Server server;

    std::unique_ptr<buildboxcommon::KeyValueStorageClient> client;
};

TEST_F(ServerTestFixture, TestPutGet)
{
    ASSERT_TRUE(client->putKey("foo", "bar").ok());

    std::string value;
    ASSERT_TRUE(client->getKey("foo", &value).ok());
    ASSERT_EQ(value, "bar");
}

TEST_F(ServerTestFixture, TestGetNonExisting)
{
    std::string value;
    ASSERT_EQ(client->getKey("non-existing-key", &value).error_code(),
              grpc::StatusCode::NOT_FOUND);
}
