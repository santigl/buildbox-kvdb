/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXKVDB_KEYVALUESTORAGE
#define INCLUDED_BUILDBOXKVDB_KEYVALUESTORAGE

#include <chrono>
#include <memory>

namespace buildboxkvdb {

class KeyValueStorage {
  public:
    typedef std::string KeyType;
    typedef std::string ValueType;
    typedef std::chrono::seconds TtlType;

    virtual std::unique_ptr<ValueType> getKey(const KeyType &) = 0;

    virtual void putKey(const KeyType &key, const ValueType &value) = 0;

    virtual void putKey(const KeyType &key, const ValueType &value,
                        const TtlType &ttl) = 0;

    virtual ~KeyValueStorage() = default;
};
} // namespace buildboxkvdb

#endif
