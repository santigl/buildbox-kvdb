/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_commandline.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>

#include <buildboxcommon_keyvaluestorageclient.h>

#include <iostream>

int main(int argc, char *argv[])
{
    buildboxcommon::logging::Logger::getLoggerInstance().initialize(argv[0]);

    if (argc < 4 || argc > 6) {
        std::cerr << "error, missing args" << std::endl;
    }

    const std::string address = argv[1];
    const std::string operation = argv[2];
    const std::string key = argv[3];

    int ttlSecs = 5 * 60;
    if (argc == 6) {
        try {
            ttlSecs = std::stoi(argv[5]);
        }
        catch (const std::invalid_argument &) {
            BUILDBOX_LOG_ERROR("Invalid TTL value: \"" << argv[6] << "\"");
        }
        catch (const std::out_of_range &) {
            BUILDBOX_LOG_ERROR("TTL out of range");
        }
    }

    buildboxcommon::ConnectionOptions connectionOptions;
    connectionOptions.setUrl(address);

    buildboxcommon::KeyValueStorageClient client(connectionOptions);

    if ((argc == 4 && operation != "get") ||
        (argc == 5 && operation != "put")) {
        std::cerr << "Invalid arguments" << std::endl;
        return 1;
    }

    grpc::Status status;
    if (operation == "get") {
        std::string value;
        status = client.getKey(key, &value);
        if (status.ok()) {
            std::cout << value << std::endl;
        }
    }
    else {
        const std::string value = argv[4];
        status = client.putKey(key, value, ttlSecs);
    }

    if (status.ok()) {
        return 0;
    }

    BUILDBOX_LOG_ERROR(operation << " failed with error ["
                                 << status.error_code() << "] ("
                                 << status.error_details() << ")");
    return 1;
}
