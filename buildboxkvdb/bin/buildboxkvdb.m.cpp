/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxkvdb_cmdlinespec.h>

#include <buildboxcommon_logging.h>
#include <buildboxkvdb_keyvalueinmemorystorage.h>
#include <buildboxkvdb_server.h>

#include <csignal>
#include <iostream>

namespace {
volatile std::sig_atomic_t signalValue;

void signalHandler(int signal) { signalValue = signal; }

} // namespace

int main(int argc, char *argv[])
{
    const std::string defaultBindAddress = "localhost:60000";

    const std::string defaultLogLevel =
        buildboxcommon::logging::logLevelToString.at(
            buildboxcommon::LogLevel::ERROR);

    const buildboxkvdb::CmdLineSpec spec(defaultBindAddress, defaultLogLevel);

    buildboxcommon::logging::Logger::getLoggerInstance().initialize(argv[0]);

    buildboxcommon::CommandLine commandLine(spec.d_spec);
    const bool success = commandLine.parse(argc, argv);
    if (!success) {
        commandLine.usage();
        return EXIT_FAILURE;
    }

    if (commandLine.exists("help")) {
        commandLine.usage();
        return 0;
    }

    if (commandLine.exists("version")) {
        std::cout << argv[0] << " " << BUILDBOX_KVDB_VERSION << std::endl;
        return 0;
    }

    auto logLevel = buildboxcommon::logging::stringToLogLevel.find(
        commandLine.getString("log-level", defaultLogLevel));
    if (logLevel != buildboxcommon::logging::stringToLogLevel.cend()) {
        BUILDBOX_LOG_SET_LEVEL(logLevel->second);
    }
    else {
        std::cerr << "Invalid log level \""
                  << commandLine.getString("log-level") << "\"" << std::endl;
        return EXIT_FAILURE;
    }

    const auto address = commandLine.getString("bind", defaultBindAddress);
    buildboxkvdb::InMemoryKeyValueStorage storage;
    buildboxkvdb::KeyValueStorageServicer servicer(&storage);

    buildboxkvdb::Server server(&storage);
    server.addListeningPort(address);

    server.start();
    BUILDBOX_LOG_INFO("Server listening on [" << address << "]");

    std::signal(SIGINT, signalHandler);
    std::signal(SIGTERM, signalHandler);

    pause();

    BUILDBOX_LOG_INFO("Received signal [" << signalValue
                                          << "]: stopping and exiting");
    server.shutdown();
    server.wait();

    return 0;
}
