/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxkvdb_keyvaluestorageservicer.h>

#include <buildboxcommon_logging.h>

namespace buildboxkvdb {

KeyValueStorageServicer::KeyValueStorageServicer(KeyValueStorage *storage)
    : d_storage(storage)
{
}

grpc::Status
KeyValueStorageServicer::GetKey(grpc::ServerContext *,
                                const build::buildbox::GetKeyRequest *request,
                                build::buildbox::GetKeyResponse *response)
{

    auto value = d_storage->getKey(request->key());
    if (value == nullptr) {
        return grpc::Status(grpc::StatusCode::NOT_FOUND, "Key not found");
    }

    response->set_value(*value);
    return grpc::Status::OK;
}

grpc::Status
KeyValueStorageServicer::PutKey(grpc::ServerContext *,
                                const build::buildbox::PutKeyRequest *request,
                                build::buildbox::PutKeyResponse *)
{
    BUILDBOX_LOG_DEBUG("PutKeyRequest: key=\""
                       << request->key() << "\", value=\"" << request->value()
                       << "\", ttl_secs=" << request->ttl().seconds());

    if (request->ttl().seconds() > 0) {
        d_storage->putKey(request->key(), request->value(),
                          KeyValueStorage::TtlType(request->ttl().seconds()));
    }
    else {
        d_storage->putKey(request->key(), request->value());
    }

    return grpc::Status::OK;
}

} // namespace buildboxkvdb
