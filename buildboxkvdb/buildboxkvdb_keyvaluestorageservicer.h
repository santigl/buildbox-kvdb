/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXKVDB_KEYVALUESTORAGESERVICER
#define INCLUDED_BUILDBOXKVDB_KEYVALUESTORAGESERVICER

#include <grpc/grpc.h>
#include <grpcpp/server_context.h>

#include <build/buildbox/kv_store.grpc.pb.h>

#include <buildboxkvdb_keyvaluestorage.h>

namespace buildboxkvdb {

class KeyValueStorageServicer final
    : public build::buildbox::KeyValueStorage::Service {
  public:
    KeyValueStorageServicer(KeyValueStorage *storage);

    grpc::Status GetKey(grpc::ServerContext *context,
                        const build::buildbox::GetKeyRequest *request,
                        build::buildbox::GetKeyResponse *response) override;

    grpc::Status PutKey(grpc::ServerContext *context,
                        const build::buildbox::PutKeyRequest *request,
                        build::buildbox::PutKeyResponse *response) override;

  private:
    KeyValueStorage *d_storage;
};

} // namespace buildboxkvdb

#endif
