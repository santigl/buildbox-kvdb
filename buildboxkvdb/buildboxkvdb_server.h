/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXKVDB_SERVER
#define INCLUDED_BUILDBOXKVDB_SERVER

#include <buildboxkvdb_keyvaluestorage.h>
#include <buildboxkvdb_keyvaluestorageservicer.h>

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

#include <memory>

namespace buildboxkvdb {
class Server final {
  public:
    explicit Server(KeyValueStorage *storage);
    ~Server();

    void start();

    void shutdown();

    void wait();

    void addListeningPort(const std::string &address);

  private:
    KeyValueStorage *d_storage;

    std::unique_ptr<KeyValueStorageServicer> d_servicer;
    grpc::ServerBuilder d_server_builder;
    std::shared_ptr<grpc::Server> d_server;
};
} // namespace buildboxkvdb

#endif
