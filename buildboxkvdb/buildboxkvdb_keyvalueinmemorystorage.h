/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXKVDB_INMEMORYKEYVALUESTORAGE
#define INCLUDED_BUILDBOXKVDB_INMEMORYKEYVALUESTORAGE

#include <buildboxkvdb_keyvaluestorage.h>

#include <mutex>
#include <unordered_map>

namespace buildboxkvdb {
class InMemoryKeyValueStorage : public KeyValueStorage {
  public:
    InMemoryKeyValueStorage();

    ~InMemoryKeyValueStorage() override {}

    std::unique_ptr<KeyValueStorage::ValueType>
    getKey(const KeyType &key) override;

    void putKey(const KeyValueStorage::KeyType &key,
                const KeyValueStorage::ValueType &value) override;

    void putKey(const KeyValueStorage::KeyType &key,
                const KeyValueStorage::ValueType &value,
                const TtlType &ttl) override;

    inline size_t size() const { return d_storage.size(); }

    // Delete entries that are expired from the storage.
    // (`protected` in order to unit test)
    void deleteExpiredEntries();

  private:
    size_t d_insertionsSinceLastCleanup;

    typedef std::chrono::system_clock::time_point Timestamp;

    struct StorageValue {
        explicit StorageValue(const ValueType &value_,
                              const Timestamp &creationTime_,
                              const TtlType &ttl_)
            : value(value_), creationTime(creationTime_), ttl(ttl_)
        {
        }

        const ValueType value;
        const Timestamp creationTime;
        const TtlType ttl;
    };

    typedef std::unordered_map<KeyValueStorage::KeyType, StorageValue>
        InMemoryMap;
    InMemoryMap d_storage;
    std::mutex d_storageMutex;

    /*
     * Return the current time.
     */
    static inline Timestamp now() { return std::chrono::system_clock::now(); }

    /*
     * Given a timestamp, compare it against the present time and return
     * whether the difference is larger than the configured time to live.
     */
    static inline bool entryHasExpired(const StorageValue &value)
    {
        return (now() - value.creationTime) > value.ttl;
    }

    static const std::chrono::seconds s_defaultTtl;

    static const size_t s_cleanupPeriodInsertions;
};

} // namespace buildboxkvdb

#endif
