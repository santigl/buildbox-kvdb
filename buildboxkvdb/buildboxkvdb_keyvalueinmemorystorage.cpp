/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxkvdb_keyvalueinmemorystorage.h>

namespace buildboxkvdb {

const std::chrono::seconds InMemoryKeyValueStorage::s_defaultTtl(3);
const size_t InMemoryKeyValueStorage::s_cleanupPeriodInsertions(1024);

InMemoryKeyValueStorage::InMemoryKeyValueStorage()
    : d_insertionsSinceLastCleanup(0)
{
}

std::unique_ptr<KeyValueStorage::ValueType>
InMemoryKeyValueStorage::getKey(const KeyValueStorage::KeyType &key)
{
    const std::lock_guard<std::mutex> lock(d_storageMutex);

    const auto entryIt = d_storage.find(key);
    if (entryIt != d_storage.end()) {
        if (!entryHasExpired(entryIt->second)) {
            return std::make_unique<KeyValueStorage::ValueType>(
                entryIt->second.value);
        }

        d_storage.erase(entryIt);
    }

    return nullptr;
}

void InMemoryKeyValueStorage::putKey(const KeyValueStorage::KeyType &key,
                                     const KeyValueStorage::ValueType &value)
{
    putKey(key, value, s_defaultTtl);
}

void InMemoryKeyValueStorage::putKey(const KeyValueStorage::KeyType &key,
                                     const KeyValueStorage::ValueType &value,
                                     const KeyValueStorage::TtlType &ttl)
{
    {
        const std::lock_guard<std::mutex> lock(d_storageMutex);

        auto it = d_storage.find(key);
        if (it != d_storage.end()) {
            d_storage.erase(it);
        }

        d_storage.emplace(key, StorageValue(value, now(), ttl));
    }

    if (++d_insertionsSinceLastCleanup == s_cleanupPeriodInsertions) {
        deleteExpiredEntries();
        d_insertionsSinceLastCleanup = 0;
    }
}

void buildboxkvdb::InMemoryKeyValueStorage::deleteExpiredEntries()
{
    const std::lock_guard<std::mutex> lock(d_storageMutex);
    for (auto it = d_storage.begin(); it != d_storage.end();) {
        if (entryHasExpired(it->second)) {
            it = d_storage.erase(it);
        }
        else {
            it++;
        }
    }
}

} // namespace buildboxkvdb
