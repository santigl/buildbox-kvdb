/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxkvdb_server.h>

#include <buildboxcommon_exception.h>

namespace buildboxkvdb {

Server::Server(KeyValueStorage *storage) : d_storage(storage) {}

Server::~Server() {}

void Server::start()
{
    if (d_server != nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::logic_error,
                                       "Cannot start server twice");
    }

    d_servicer = std::make_unique<KeyValueStorageServicer>(d_storage);
    d_server_builder.RegisterService(d_servicer.get());

    d_server = d_server_builder.BuildAndStart();
    if (d_server == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Failed to start server");
    }
}

void Server::shutdown() { d_server->Shutdown(); }

void Server::wait() { d_server->Wait(); }

void Server::addListeningPort(const std::string &address)
{
    if (d_server != nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::logic_error,
            "Cannot add listening port after starting server");
    }

    d_server_builder.AddListeningPort(address,
                                      grpc::InsecureServerCredentials());
}

} // namespace buildboxkvdb
