/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxkvdb_cmdlinespec.h>

namespace buildboxkvdb {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

CmdLineSpec::CmdLineSpec(const std::string &defaultBindAddress,
                         const std::string &defaultLogLevel)
{
    d_spec.emplace_back(
        "bind", "Bind to address:port or UNIX socket in unix:path",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(defaultBindAddress));

    d_spec.emplace_back("default-ttl", "Default TTL in seconds for entries",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("log-level", "Logging verbosity level",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue(defaultLogLevel));

    d_spec.emplace_back("version", "Print version information and exit",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));

    d_spec.emplace_back("help", "Display usage and exit",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));
}

} // namespace buildboxkvdb
