# buildbox-kvdb

Minimal key-value storage server that listens on a Unix domain socket and
services `Put/GetKey()` gRPC requests as defined in
[`build/buildbox/kv_store.proto`](https://gitlab.com/BuildGrid/buildbox/buildbox-common/-/blob/santigl/add-kv-storage-proto/protos/build/buildbox/kv_store.proto) (not yet merged).

Its goal is to be used as a cache from tools like
[`recc`](https://gitlab.com/bloomberg/recc), where a short-lived invocation
might want to store computed or fetched values to avoid network trips in
subsequent ones.

It depends on [buildbox-common](https://gitlab.com/BuildGrid/buildbox/buildbox-common).

## Building

```shell
mkdir -p build/ && cmake .. && make
```

## Using
### Starting server

```shell
> ./buildboxkvdb/buildbox-kvdb --bind "unix:s.sock" --log-level=debug
```

### Adding a key/value

```shell
> ./buildboxkvdb/buildbox-kvdb-client "unix:s.sock" put "hello" "world"
```

### Reading a value

```shell
> ./buildboxkvdb/buildbox-kvdb-client "unix:s.sock" get "hello"
world
```
